public class Student {
    private String name;
    private double pcm;
    private double cs;
    private double maths;
    private double total_marks;

    public Student(String name, double pcm, double cs, double maths) {
        this.name = name;
        this.pcm = pcm;
        this.cs = cs;
        this.maths = maths;
        this.total_marks = pcm + cs + maths;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPcm() {
        return this.pcm;
    }

    public void setPcm(double pcm) {
        this.pcm = pcm;
    }

    public double getCs() {
        return this.cs;
    }

    public void setCs(double cs) {
        this.cs = cs;
    }

    public double getMaths() {
        return this.maths;
    }

    public void setMaths(double maths) {
        this.maths = maths;
    }

    public double getTotalMarks() {
        return this.total_marks;
    }
}
