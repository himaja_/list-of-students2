import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class ListsOfStudentsGenerator {

    public static ArrayList<Student> readData(int number_of_students) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Student> students = new ArrayList<>();
        Student student;
        String name;
        double pcm, cs, maths;
        for (int i = 0; i < number_of_students; i++) {
            System.out.println("\n***** Enter the details of student-" + (i + 1) + " *****");
            System.out.print("Name: ");
            name = scan.next();
            System.out.print("Marks of PCM: ");
            pcm = scan.nextDouble();
            System.out.print("Marks of CS: ");
            cs = scan.nextDouble();
            System.out.print("Marks of Mathematics: ");
            maths = scan.nextDouble();
            student = new Student(name, pcm, cs, maths);
            students.add(student);
        }
        return students;
    }

    public static void createAndPrintLists(ArrayList<Student> students) {

        ArrayList<String> computer_science = new ArrayList<>();
        ArrayList<String> biology = new ArrayList<>();
        ArrayList<String> commerce = new ArrayList<>();
        for (Student s : students) {
            if (s.getPcm() > 70 && s.getCs() > 80)
                computer_science.add(s.getName());
            if (s.getPcm() > 70)
                biology.add(s.getName());
            if (s.getMaths() > 80)
                commerce.add(s.getName());
        }

        System.out.println("\n*****List of Students that can be put into different groups*****");
        System.out.println("Computer Science: " + computer_science);
        System.out.println("Biology: " + biology);
        System.out.println("Commerce: " + commerce);

    }

    public static void printRanksOfStudents(ArrayList<Student> students) {

        int number_of_students = students.size();

        for (int i = 0; i < number_of_students - 1; i++)
            for (int j = 0; j < number_of_students - i - 1; j++)
                if (students.get(j).getTotalMarks() < students.get(j + 1).getTotalMarks()) {
                    Student temp = students.get(j);
                    students.set(j, students.get(j + 1));
                    students.set(j + 1, temp);
                }

        HashMap<String, Integer> ranks = new HashMap<>();
        int rank = 1;
        ranks.put(students.get(0).getName(), rank);
        for (int i = 1; i < number_of_students; i++) {
            if (students.get(i).getTotalMarks() != students.get(i - 1).getTotalMarks())
                rank += 1;
            ranks.put(students.get(i).getName(), rank);
        }

        System.out.println(ranks);

    }

    public static void main(String[] args) {
        int number_of_students;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        number_of_students = scan.nextInt();
        ArrayList<Student> students;
        students = readData(number_of_students);
        createAndPrintLists(students);
        printRanksOfStudents(students);
    }
}
